import unittest
from rectangulo import Rectangulo
#from rectagulo import rectangulo -> solo coge class rectangulo
#from rectangulo import cuadrado -> solo coge class cuadrado


class RectanguloTestCase(unittest.TestCase):
    def test_perimetro(self):
        rectangulo1 = Rectangulo(1, 2)
        perimetro =rectangulo1.perimetro()
        self.assertEqual(perimetro, 6)
#Hay que poner test al principio para que ejecute el test
    def test_area(self):
        rectangulo1 = Rectangulo (1, 2)
        area = rectangulo1.area()
        self.assertEqual(area, 2 )


unittest.main()
if __name__ == '__main__':
    unittest.main()
