
class Empleado:
    def __init__(self, nombre, salario, tasa, antiguedad):
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__ antiguedad = antiguedad

    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre, tax=self.__impuestos))
        return self.__impuestos

    def ImprimirAntiguedad (self):
        print("Antiguedad de " + self.__nombre + ":" +self.__antiguedad)
    
    def AhorroImpuestos(self):
        if(self.__ antiguedad > 1):
            print("Empleado con más de un año de antiguedad")
    def displayCost(total):
        print("Los impuestos a pagar en total son {:.2f} euros".format(total))

emp1 = Empleado("Pepe", 20000, 0.35)
print(emp1.nombre)
emp1.ImprimirAntiguedad()
emp2 = Empleado("Ana", 30000, 0.30)

empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10), Empleado("Luisa", 25000, 0.15)]
total = 0
for emp in empleados:
    total += emp.CalculoImpuestos()
displayCost(total)