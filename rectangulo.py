import unittest
class Rectangulo:
    def __init__(self, l1, l2):
        self.l1 = l1
        self.l2 =l2

    def perimetro(self):
        return self.l1*2 + self.l2*2
        
    def area(self):
        return self.l1*self.l2

    def __str__(self):
        return"El perímetro del rectángulo es {p} y el area es {a}".format(p=self.perimetro(), a=self.area())

rectangulo1= Rectangulo(1, 2)
print(rectangulo1)
