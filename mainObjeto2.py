class Empleado:
    """ Un ejemplo de clase para empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s
empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana", 30000)
print("El nombre del empleado es {} y su salario es {}".format(empleadoPepe.nombre, empleadoPepe.nomina))