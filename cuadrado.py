class Cuadrado:
    def __init__(self, l):
        self.lado = l

    def perimetro(self):
        return self.lado*4

    def area(self):
        return self.lado**2

    def __str__(self):
        return"El perímetro del cuadrado es {p} y el area es {a}".format(p=self.perimetro(), a=self.area())

cuadrado1= Cuadrado(3)
print(cuadrado1)